package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }

    // TODO Complete me!
    public Duck() {
    }

    // public FlyBehavior getFlyBehavior() {
    // return flyBehavior;
    // }

    // public QuackBehavior getQuackBehavior() {
    // return quackBehavior;
    // }

    public void setFlyBehavior(FlyBehavior fb) {
        this.flyBehavior = fb;
    }

    public void setQuackBehavior(QuackBehavior qb) {
        this.quackBehavior = qb;
    }

    public abstract void display();

    public void swim() {
        System.out.println("semua burung mengapung, even burung decoy :v");
    }
}
