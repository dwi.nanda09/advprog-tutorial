package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.stream.Stream;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        /*
         * Before using stream API for (int i =0; i < this.commands.size(); i++){
         * 
         * commands.get(i).execute(); }
         */
        // Using stream API
        this.commands.stream().forEach(command -> command.execute());

    }

    @Override
    public void undo() {
        // TODO Complete me!
        for (int i = this.commands.size() - 1; i >= 0; i--) {
            this.commands.get(i).undo();
        }

    }
}
