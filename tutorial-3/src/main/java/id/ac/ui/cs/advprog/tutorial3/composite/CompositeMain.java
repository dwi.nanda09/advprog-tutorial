package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;

import java.util.List;

public class CompositeMain {
    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee(new Ceo("Wawan", 500000));
        company.addEmployee(new Cto("Asep", 300000));

        company.addEmployee(new BackendProgrammer("Nugraha", 200000));
        company.addEmployee(new FrontendProgrammer("Angga", 200000));

        List<Employees> listEmployees = company.getAllEmployees();

        System.out.println("List of All Employees");
        for (int i = 0; i < listEmployees.size(); i++) {
            System.out.println(listEmployees.get(i).getName());
        }
        System.out.printf("Total salary = %.2f\n", company.getNetSalaries());
    }
}
