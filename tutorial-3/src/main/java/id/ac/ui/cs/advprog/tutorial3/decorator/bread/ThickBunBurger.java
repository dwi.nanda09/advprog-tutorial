package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThickBunBurger extends Food {
    String description;

    public ThickBunBurger() {
        //TODO Implement
        this.description = "Thick Bun Burger";
    }

    @Override
    public double cost() {
        //TODO Implement
        return 2.5;
    }

    public String getDescription() {
        return this.description;
    }
}
