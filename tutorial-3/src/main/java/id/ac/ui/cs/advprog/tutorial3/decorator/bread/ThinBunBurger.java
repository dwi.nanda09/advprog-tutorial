package id.ac.ui.cs.advprog.tutorial3.decorator.bread;

import id.ac.ui.cs.advprog.tutorial3.decorator.Food;

public class ThinBunBurger  extends Food {
    String description;

    public ThinBunBurger() {
        //TODO Implement
        this.description = "Thin Bun Burger";
    }

    @Override
    public double cost() {
        //TODO Implement
        return 1.5;
    }

    public String getDescription() {
        return this.description;
    }
}
