package hello;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class CurriculumVitaeController {

    @GetMapping("/curriculum-vitae")
    public String greeting(@RequestParam(name = "visitor", required = true, defaultValue = "")
                                   String visitor, Model model) {

        // this is a comment
        String title;
        if (visitor.length() != 0) {
            title = visitor + ", I hope you interested to hire me";
        } else {
            title = "This is my CV";
        }

        model.addAttribute("title", title);
        return "curriculum_vitae";
    }

}
