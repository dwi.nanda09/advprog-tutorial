package hello;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CurriculumVitaeController.class)
public class CurriculumVitaeTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void curriculumVitaeWithUser() throws Exception {
        mockMvc.perform(get("/curriculum-vitae").param("visitor", "Lala"))
                .andExpect(content().string(containsString(
                        "Lala, I hope you interested to hire me")));
    }

    @Test
    public void curriculumVitaeNoUser() throws Exception {
        mockMvc.perform(get("/curriculum-vitae"))
                .andExpect(content().string(containsString("This is my CV")));
    }

}
